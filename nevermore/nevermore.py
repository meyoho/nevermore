'''
Created on 2016-02-23

@author: sincera
'''

import sys
import fluentd
import fluentd_crio
import fluentd_monitor
import fluentd_match


def diagnose():
    pass


def garbage_collect():
    pass


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: python nevermore [action]. Action: fluent|gc|hc"
        sys.exit()
    if sys.argv[1] == "fluent":
        obj = fluentd.FluentConfigGenerator()
        obj.run()
    if sys.argv[1] == "fluent_crio":
        obj = fluentd_crio.FluentConfigGenerator()
        obj.run()
    if sys.argv[1] == "fluent_monitor":
        obj = fluentd_monitor.FluentMonitorHandler()
        obj.run()
    if sys.argv[1] == "fluent_match":
        obj = fluentd_match.FluentOutputHandler()
        obj.run()
