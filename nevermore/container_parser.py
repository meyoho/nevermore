'''
Created on 2017-06-23
Update on 2019-02-24
@author: Sincera,lmxia@alauda.io

'''
import logging
import os
import json
import base64
import settings
LOG = logging.getLogger(__name__)


class ContainerParser(object):
    def __init__(self, docker_client):
        self.docker_client = docker_client

    def get_file_log_path(self, envs):
        file_path = envs.get(settings.FILE_LOG_KEY)
        if not file_path:
            return []
        return list(set(file_path.split(",")))

    def get_exclude_log_path(self, envs):
        exclude_path = envs.get(settings.EXCLUDE_FILE_LOG_KEY)
        if not exclude_path:
            return []
        return list(set(exclude_path.split(",")))

    def filter_exclude_path(self, file_paths, exclude_paths):
        intersection = set(file_paths) & set(exclude_paths)
        return list(set(file_paths) - intersection)

    def get_all_containers_ids(self):
        ids = [x["Id"] for x in self.docker_client.get_all_containers(quiet=True)]
        all_ids = {}
        for id in ids:
            if self.is_pause_container(id):
                continue
            encoding = self.get_log_encoding(id)
            result = {
                "encoding": encoding
            }
            all_ids[id] = result
        return all_ids

    def get_log_encoding(self, id):
        envs = self.docker_client.get_environments(id)
        log_encoding = envs.get("LOG_ENCODING", "UTF-8")
        if log_encoding not in settings.ENCODING_LIST:
            log_encoding = "UTF-8"
        return log_encoding

    def is_pause_container(self, id):
        labels = self.docker_client.get_labels(id)
        return labels.get("io.kubernetes.docker.type") != "container"

    def get_tail_info(self, id):
        detail = self.docker_client.retrieve_container(id)
        envs = self.docker_client.get_environments(id, detail)
        include_paths = self.get_file_log_path(envs)
        exclude_paths = self.get_exclude_log_path(envs)
        file_paths = self.filter_exclude_path(include_paths, exclude_paths)

        if not file_paths:
            LOG.debug("Ignore container: {}. env: {}".format(detail["Name"], envs))
            return None

        log_encoding = envs.get("LOG_ENCODING", "UTF-8")
        if log_encoding not in settings.ENCODING_LIST:
            log_encoding = "UTF-8"
        result = {
            "encoding" : log_encoding,
            "include": [],
            "exclude": ""
        }
        excludes = []
        storage_driver = self.get_storage_driver()
        if storage_driver == 'overlay2':
            host_base_path = detail['GraphDriver']['Data']['UpperDir']
            host_base_path = '/scan_dir' + host_base_path
        elif storage_driver == 'btrfs':
            container_layer = "/scan_dir/var/lib/docker/image/btrfs/layerdb/mounts/{}/mount-id".format(id)
            with open(container_layer, 'r') as f:
                container_layer_id = f.read()
            host_base_path = "/scan_dir/var/lib/docker/btrfs/subvolumes/{}".format(container_layer_id)
        else:
            pass

        mount_info = self.docker_client.get_mounts(id, info=detail)
        for file_logs in file_paths:
            file_info = os.path.split(file_logs)
            # not user mount
            if file_info[0] not in mount_info:
                if host_base_path:
                    full_path = host_base_path + file_logs
                    real_file_info = os.path.split(full_path)
                    result["include"].append({
                        "real_path": file_logs,
                        "full_path": full_path,
                        "base64_code": base64.b64encode(real_file_info[0])
                    })
                    for exclude_file in exclude_paths:
                        if exclude_file.startswith(file_info[0]):
                            exclude_full_path = "{}{}".format(host_base_path, exclude_file)
                            if exclude_full_path not in excludes:
                                excludes.append(exclude_full_path)
                else:
                    continue
            else:
                first_part = mount_info.get(file_info[0]).get('Source')
                first_part = "/scan_dir" + first_part
                full_path = first_part + "/" + file_info[1]
                result["include"].append({
                    "real_path": file_logs,
                    "full_path": full_path,
                    "base64_code": base64.b64encode(first_part)
                })

                for exclude_file in exclude_paths:
                    if exclude_file.startswith(file_info[0]):
                        exclude_file_split = os.path.split(exclude_file)
                        exclude_full_path = "{}{}".format(first_part, exclude_file_split[1])
                        if exclude_full_path not in excludes:
                            excludes.append(exclude_full_path)
        # has no include setting
        if not result.get("include"):
            return None
        result["exclude"] = json.dumps(excludes)
        return result

    def get_existed_container_info(self):
        ids = [x["Id"] for x in self.docker_client.get_all_containers(quiet=True)]
        result = {}
        for id in ids:
            info = self.get_tail_info(id)
            if not info:
                continue
            result[id] = info
        return result, ids

    def get_omit_containers_info(self, existed_ids):
        ids = [x["Id"] for x in self.docker_client.get_all_containers(quiet=True)]
        omits = list(set(ids) - (set(ids) & set(existed_ids)))
        result = {}
        LOG.info("Omit containers: {}".format(omits))
        for id in omits:
            info = self.get_tail_info(id)
            if not info:
                continue
            result[id] = info
        return result, omits

    def get_log_driver(self):
        return self.docker_client.get_log_driver()

    def get_storage_driver(self):
        return self.docker_client.get_storage_driver()
