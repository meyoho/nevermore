import docker
from docker_base import DockerBaseClient


class DockerClient(DockerBaseClient):
    def __init__(self, base_url, timeout):
        self.docker_version = "auto"
        super(DockerClient, self).__init__(base_url, timeout)
        self.api_client = docker.APIClient(
            base_url=base_url,
            version=self.docker_version,
            timeout=timeout
        )


def get_docker_client(docker_path, timeout):
    return DockerClient(docker_path, timeout)
