class DockerBaseClient(object):
    def __init__(self, base_url, timeout):
        self.base_url = base_url
        self.version = None
        self.api_client = None
        self.client = None

    def watch_event(self):
        return self.api_client.events()

    def get_container_id(self, event):
        return event["id"]

    def get_all_containers(self, quiet=True):
        return self.api_client.containers(quiet=quiet)

    def retrieve_container(self, id):
        return self.api_client.inspect_container(id)

    def get_log_driver(self):
        docker_info = self.api_client.info()
        return docker_info['LoggingDriver']

    def get_storage_driver(self):
        docker_info = self.api_client.info()
        return docker_info['Driver']

    def get_environments(self, id, info=None):
        if not info:
            info = self.retrieve_container(id)
        envs = info["Config"]["Env"]
        result = {}
        if envs is None:
            return result
        for env in envs:
            s = env.split("=")
            result[s[0]] = s[1]
        return result

    def get_labels(self, id):
        info = self.retrieve_container(id)
        labels = info["Config"]["Labels"]
        return labels

    def get_mounts(self, id, info=None):
        if not info:
            info = self.retrieve_container(id)
        mounts = info["Mounts"]
        result = {}
        for mount in mounts:
            result[mount["Destination"]] = mount
        return result
