from templates.template_fluentd import cfg as fluentd_cfg, match_cfg as fluentd_match_cfg
from templates.template_jsonfile import cfg as jsonfile_cfg, match_cfg as jsonfile_match_cfg
from docker_client.docker_factory import get_docker_client
from settings import DOCKER_PATH

docker_info = dict()
cfg = None
match_cfg = None

try:
    docker_client = get_docker_client(DOCKER_PATH, 10)
    docker_info = docker_client.api_client.info()
except:
    pass


if docker_info.get('LoggingDriver') == 'fluentd':
    cfg = fluentd_cfg
    match_cfg = fluentd_match_cfg
else:
    cfg = jsonfile_cfg
    match_cfg = jsonfile_match_cfg
