'''
Created on 2016-02-23

@author: sincera
'''
import logging
import commands
import time

LOG = logging.getLogger(__name__)


class NevermoreException(Exception):
    def __init__(self, message):
        self.message

    def __str__(self):
        return "[NEVERMORE]: {}".format(self.message)


class NevermoreInvalidVersion(NevermoreException):
    def __str__(self):
        return "[NEVERMOREINVALIDVERSION]: Not support docker version: {}".format(self.message)


def restart():
    mode = "restart"
    res = commands.getstatusoutput('service td-agent {}'.format(mode))
    for i in range(3):
        if res[0] != 0:
            LOG.error('{}st time  td-agent fail! error: {}'.format(mode, i, res))
            time.sleep(1)
            continue
        LOG.info("Restart td-agent successfully")
        return
