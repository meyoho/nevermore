import logging
import time
import os
from settings import MONITOR_INTERVAL


LOG = logging.getLogger(__name__)
FLUENT_MONITOR_FILE_PTAH = "/tmp/fluent_monitor"
MONITOR_COMMAND = "ps aux | grep '/opt/td-agent/embedded/bin/ruby " \
                  "/usr/sbin/td-agent --log /var/log/td-agent/td-agent.log'" \
                  " | grep -v grep > {}".format(FLUENT_MONITOR_FILE_PTAH)
RESTART_FLUENT_COMMAND = "service td-agent restart"


class FluentMonitorHandler(object):
    def __init__(self):
        pass

    def run(self):
        while True:
            LOG.debug("monitor fluent")
            os.system(MONITOR_COMMAND)
            if not os.path.getsize(FLUENT_MONITOR_FILE_PTAH):
                LOG.error("fluent process die, restart it")
                os.system(RESTART_FLUENT_COMMAND)
            time.sleep(float(MONITOR_INTERVAL))
