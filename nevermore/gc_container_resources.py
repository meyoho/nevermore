#!/usr/bin/env python
import os
import sys
import time
import Queue
import shutil
import logging
import threading
from docker_client.docker_factory import get_docker_client

logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format="%(asctime)s %(message)s")
LOG = logging.getLogger(__name__)

POS_BASE_PATH = "/scan_dir/alauda/log_pos/containers"

DOCKER_PATH = os.getenv('DOCKER_PATH', 'unix://scan_dir/run/docker.sock')
GC_WORKERS = os.getenv("GC_WORKERS", 5)
GC_INTERVAL = os.getenv("GC_INTERVAL", 1800)

EXIT = False


class Checker(threading.Thread):
    def __init__(self, docker_client, queue):
        threading.Thread.__init__(self)
        self.docker_client = docker_client
        self.queue = queue
        self._queue = Queue.Queue()
        self.interval = (int(GC_INTERVAL) or 1800) / 2
        self.all_container_ids = dict()

    def get_all_containers_ids(self):
        return {x["Id"]: 1 for x in self.docker_client.get_all_containers(quiet=True)}

    def check(self, base_path, container_id):
        path = os.path.join(base_path, container_id)
        if (os.path.exists(path) and os.path.isdir(path) and
                container_id not in self.all_container_ids):
            LOG.info("Put {} to internal queue as dir {} exist".format(container_id, path))
            self._queue.put_nowait(container_id)

    def run(self):
        while True:
            if EXIT:
                LOG.info("Checker {} exit".format(self.name))
                break
            self.all_container_ids = self.get_all_containers_ids()

            while not self._queue.empty():
                container_id = self._queue.get()
                if container_id not in self.all_container_ids:
                    LOG.info("Put {} to removal queue".format(container_id))
                    self.queue.put_nowait(container_id)

            # worker will has time to remove the directory
            time.sleep(self.interval)

            container_ids = os.listdir(POS_BASE_PATH)
            for container_id in container_ids:
                self.check(POS_BASE_PATH, container_id)

            time.sleep(self.interval)


class Gcer(threading.Thread):
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    @staticmethod
    def gc(base_path, container_id):
        path = os.path.join(base_path, container_id)
        if os.path.exists(path) and os.path.isdir(path):
            shutil.rmtree(path, ignore_errors=True)
            LOG.info('GC dir {} for container {}'.format(path, container_id))

    def run(self):
        while True:
            if EXIT:
                LOG.info("Gcer {} exit".format(self.name))
                break
            if not self.queue.empty():
                container_id = self.queue.get()
                self.gc(POS_BASE_PATH, container_id)

            time.sleep(5)


if __name__ == '__main__':
    queue = Queue.Queue()
    docker_client = get_docker_client(DOCKER_PATH, 10)

    threads = []
    t = Checker(docker_client, queue)
    threads.append(t)

    gc_workers = int(GC_WORKERS) or 5
    for i in range(gc_workers):
        t = Gcer(queue)
        threads.append(t)

    while not EXIT:
        try:
            for t in threads:
                if not t.isAlive():
                    t.start()
            time.sleep(int(GC_INTERVAL) or 1800)
        except KeyboardInterrupt:
            EXIT = True
