from jinja2 import Template


cfg = '''
<source>
    @type forward
    port 24224
</source>

<source>
    @type http
    port 24225
    bind 127.0.0.1
</source>

<source>
    @type monitor_agent
    bind 0.0.0.0
    port 24220
</source>

<source>
  @type systemd
  tag source.kubelet
  path /scan_dir/var/log/journal
  matches [{ "_SYSTEMD_UNIT": "kubelet.service" }]
  read_from_head false
  
  <storage>
    @type local
    path /scan_dir/var/log/fluentd-journald-kubelet-cursor.json
  </storage>
  
  <entry>
    fields_strip_underscores true
    fields_lowercase true
  </entry>
</source>

<source>
  @type systemd
  tag source.docker
  path /scan_dir/var/log/journal
  matches [{ "_SYSTEMD_UNIT": "docker.service" }]
  read_from_head false

  <storage>
    @type local
    path /scan_dir/var/log/fluentd-journald-docker-cursor.json
  </storage>
  
  <entry>
    fields_strip_underscores true
    fields_lowercase true
  </entry>
</source>

<filter source.*>
    @type record_transformer
    renew_record true
    keep_keys []
    enable_ruby true
    auto_typecast true
    <record>
        node "#{Socket.gethostname}"
        application_name ${tag_parts[1]}
        region_name  #ALAUDA_REGION_NAME#
        region_id  #ALAUDA_REGION_ID#
        log_data ${record["message"]}
        log_level 0
        paths "stdout"
        time ${(time.to_f * 1000000).to_i}
        root_account #NAMESPACE# 
        source ${tag_parts[1]}
        log_type log
    </record>
</filter>


<filter json.*.** docker.*.** file.*.*.*.**>
    @type docker_msg
</filter>

@include /etc/td-agent/match.conf

{% for uuid, info in all_ids.iteritems() %}
<source>
        @type tail
        path /scan_dir/var/lib/docker/containers/{{ uuid }}/{{ uuid }}-json.log
        pos_file /scan_dir/alauda/log_pos/containers/{{ uuid }}/.{{ uuid }}.pos
        tag json.{{ uuid }}.stdout.*
        refresh_interval 5s
        read_from_head true
        format none
        keep_time_key true
        message_key log
</source>

{% endfor %}

{% for id, container in containers.iteritems() %}
{% for include in container.include %}
<source>
        @type tail
        path {{ include.full_path }} {% if container.exclude|length > 0 %}
        exclude_path {{ container.exclude }} {% endif %}
        pos_file /scan_dir/alauda/log_pos/containers/{{ id }}/.{{ id }}_{{ loop.index }}.pos
        tag file.{{ id }}.{{include.base64_code}}.*
        refresh_interval 5s
        read_from_head true
        format none
        keep_time_key true
        message_key log_data
        {% if container.encoding != "UTF-8" %}
        encoding UTF-8
        from_encoding {{ container.encoding }} {% endif %}
</source>
{% endfor %}
{% endfor %}
'''


match_cfg = '''
<match json.*.** docker.*.** file.*.*.*.** source.*>
    @type rewrite_tag_filter
    <rule>
     key log_type 
     pattern log|file|std* 
     tag alauda.${tag_parts[1]}
    </rule>
    <rule>
     key source 
     pattern docker|kubelet
     tag alauda.${tag_parts[1]}
    </rule>
</match>

<match alauda.*.**>
    @type copy
{% if default %}
    <store>
        @type http_buffered
        endpoint_url #APIGATEWAY_ENDPOINT##APIGATEWAY_URL#
        http_retry_statuses 500,403,503,504
        http_read_timeout #HTTP_READ_TIMEOUT#
        http_open_timeout #HTTP_OPEN_TIMEOUT#
        authentication #AUTHENTICATION#  
        auth_token #AUTH_TOKEN#
        <buffer>
          @type file
          flush_interval #FLUSH_INTERVAL#
          retry_max_times #RETRY_LIMIT#
          retry_max_interval #RETRY_WAIT#
          queue_limit_length #BUFFER_QUEUE_LIMIT#
          chunk_limit_size #BUFFER_CHUNK_LIMIT#
          overflow_action drop_oldest_chunk
          path /scan_dir/var/log/buffer/alauda/
          flush_thread_count 4
        </buffer>
        <secondary>
        @type file
        path /var/log/mathilde/backup/alauda
        </secondary>
    </store>
{%endif%}
{% if kafka|length > 0 %}
    {% for k in kafka  %}
    <store>
        @type kafka_buffered
        brokers {{ k.integrated_address }}
        default_topic {{ k.topic }}
        output_data_type json
        flush_interval #FLUSH_INTERVAL#
        get_kafka_client_log true
    </store>
    {% endfor %}
{%endif%}
{% if elasticsearch|length > 0 %}
    {% for e in elasticsearch  %}
    <store>
        @type elasticsearch_dynamic
        hosts {{ e.integrated_address }}
        index_name log-${require 'time'; Time.at(record['time']/1000000).strftime("%Y%m%d").to_s}
        type_name ${record['log_type']}
        logstash_format false
        flush_interval #FLUSH_INTERVAL#
        user {{ e.integrated_username }}
        password {{ e.integrated_password }}
    </store>
    {% endfor %}
{%endif%}

{% if logstash|length > 0 %}
    {% for l in logstash %}
    <store>
        @type jsontcp
        format json
        host {{ l.host }}
        port {{ l.port }}
        <buffer>
          @type file
          flush_interval #FLUSH_INTERVAL#
          retry_max_times #RETRY_LIMIT#
          retry_max_interval #RETRY_WAIT#
          queue_limit_length #BUFFER_QUEUE_LIMIT#
          chunk_limit_size #BUFFER_CHUNK_LIMIT#
          overflow_action drop_oldest_chunk
          path /scan_dir/buffer/logstash{{ loop.index }}/
          flush_thread_count 4
        </buffer>
    </store>
    {% endfor %}
{%endif%}

{% if fluentd_hosts|length > 0 %}
    {% for fluentd in fluentd_hosts %}
    <store>
        @type forward
        <server>
            host {{ fluentd.host }}
            port {{ fluentd.port }}
        </server>
    </store>
    {% endfor %}
{%endif%}
</match>
'''


if __name__ == "__main__":
    containers = {
        "1111111111111111111111111": {
            "uuid": '1111111x7',
            "include": [
                {
                    "full_path": "/1111111x7/var/log/a.log",
                    "base64_code": "L3Zhci9sb2c="
                },
                {
                    "full_path": "/1111111x7/abc.log",
                    "base64_code": "Lw=="
                },
                {
                    "full_path": "/1111111x7/var/log/*.log",
                    "base64_code": "L3Zhci9sb2c="
                }
            ],
            "exclude": '["/1111111x7/var/log/c.log", "/1111111x7/abcde.log"]'
        },
        "2222222222222222222222222": {
            "uuid": '2222222x7',
            "include": [
                {
                    "full_path": "/2222222x7/var/log/mathilde/a.log",
                    "base64_code": "L3Zhci9sb2cvbWF0aGlsZGU="
                },
                {
                    "full_path": "/2222222x7/etc/abc.log",
                    "bae64_code": "L2V0Yw=="
                },
                {
                    "full_path": "/2222222x7/var/log/mathilde/*.log",
                    "base64_code": "L3Zhci9sb2cvbWF0aGlsZGU="
                }
            ],
            "exclude": '["/2222222x7/var/log/mathilde/c.log", "/2222222x7/etc/abcde.log"]'
        },
        "3333333333333333333333333": {
            "uuid": '3333333x7',
            "include": [
                {
                    "full_path": "/3333333x7/var/log/nevermore/a.log",
                    "base64_code": "L3Zhci9sb2cvbmV2ZXJtb3Jl"
                },
                {
                    "full_path": "/3333333x7/usr/abc.log",
                    "base64_code": "L3Vzcg=="
                },
                {
                    "full_path": "/3333333x7/var/log/nevermore/*.log",
                    "base64_code": "L3Zhci9sb2cvbmV2ZXJtb3Jl"
                }
            ],
            "exclude": ''
        }
    }

    result = Template(cfg).render({'containers': containers,
                                   'fluentd_hosts': [
                                       {"host": "10.0.0.1",
                                        "port": "24224"},
                                       {
                                           "host": "192.168.1.1",
                                           "port": "12345"
                                       }
                                   ]
                                   })
    print result

    result = Template(match_cfg).render({
        "default": "default",
        "kafka": [
            {
                " id": "qqqqq",
                "name": "kafka01",
                "endpoint": "10.0.0.8:9092",
                "topic": "topic_name",
            }
        ],
        "elasticsearch": [
            {
                " id": "qqqqq",
                "name": "kafka01",
                "endpoint": "10.0.0.8:9200",
                "user": "123",
                "password": "456"
            },
        ]
    })

    print result
