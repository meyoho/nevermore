'''
Created on 2016-02-23

@author: sincera
'''

import logging
import os
import ast


def str2bool(v):
    return v and v.lower() in ("yes", "true", "t", "1")

def get_encoding_list():
    with open("./encoding.list", 'r') as f:
        return ast.literal_eval(f.read())

SUPPORT_DOCKER_VERSION = ["1.12"]

DOCKER_PATH = os.getenv('DOCKER_PATH', 'unix://scan_dir/run/docker.sock')

logging.basicConfig(
    level=logging.DEBUG,
    format=('%(asctime)s [%(process)d] %(levelname)s %(pathname)s' +
            ' %(name)s.%(funcName)s Line:%(lineno)d %(message)s'),
    filename=os.getenv('LOG_PATH', '/var/log/mathilde/') + 'nevermore.log',
)

CLEAN_INTERVAL = os.getenv('CLEAN_INTERVAL', 12)

FLUENTD_HOSTS = os.getenv('FLUENTD_HOSTS', '')

MONITOR_INTERVAL = os.getenv('MONITOR_INTERVAL', 5)

JAKIRO_ENDPOINT = os.getenv("JAKIRO_ENDPOINT", "http://127.0.0.1")

REFRESH_INTERVAL = os.getenv('REFRESH_INTERVAL', 10)

NAMESPACE = os.getenv('NAMESPACE')

REGION_NAME = os.getenv('REGION_NAME')

AUTH_TOKEN = os.getenv('AUTH_TOKEN')

FILE_LOG_KEY = os.getenv("__FILE_LOG_PATH_KEY__", "__FILE_LOG_PATH__")

EXCLUDE_FILE_LOG_KEY = os.getenv("__EXCLUDE_FILE_LOG_PATH_KEY__", "__EXCLUDE_LOG_PATH__")

ENCODING_LIST = get_encoding_list()