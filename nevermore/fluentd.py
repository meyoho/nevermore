'''
Created on 2016-02-23

@author: sincera
'''
import logging
from jinja2 import Template
import json
import md5
import os
import time
import requests
from template_handle import cfg
from docker_client.docker_factory import get_docker_client
from container_parser import ContainerParser
from settings import DOCKER_PATH, FLUENTD_HOSTS


DOCKER_START = "start"
DOCKER_DIE = "die"
DOCKER_OOM = "oom"
LOG = logging.getLogger(__name__)


class FluentConfigGenerator(object):
    def __init__(self):
        docker_client = get_docker_client(DOCKER_PATH, 10)
        self.parser = ContainerParser(docker_client)
        self.docker_client = docker_client
        self.all_ids = {}
        self.containers = {}
        self.existed_ids = []
        self.md5 = md5.new()
        self.log_driver = "json-file"

    def acquire_lock(self, lock):
        while os.path.exists(lock):
            LOG.info("{}. wait".format(lock))
            time.sleep(1)
        with open(lock) as f:
            f.write("{}".format(time.time()))

    def release_lock(self, lock):
        if os.path.exists(lock):
            os.remove(lock)

    def check_md5(self, md5):
        if os.path.exists("/etc/td-agent/config.md5"):
            with open("/etc/td-agent/config.md5", "rb") as f:
                old_md5 = f.readline()
                if old_md5 == md5:
                    return True
                return False
        return False

    def generate_config(self):
        fluentd_hosts = []
        fluentds = FLUENTD_HOSTS.split(',')
        for fluentd in fluentds:
            f = fluentd.split(':')
            if len(f) != 2:
                continue
            fluentd_hosts.append({
                'host': f[0],
                'port': f[1]
            })
        temp = {
            'containers': self.containers,
            'all_ids': self.all_ids
        }
        if fluentd_hosts:
            temp['fluentd_hosts'] = fluentd_hosts
        template = Template(cfg).render(temp)
        with open("/etc/td-agent/td-agent.conf", "wb") as f:
            f.write(template)

    def kill_fluent(self):
        output = os.popen(
            "ps aux | grep td-agent  | grep -v '$0' | grep -v 'grep' | awk '{print $2}'").read()
        if output:
            for pid in output.split("\n"):
                if pid:
                    os.system("kill {}".format(pid))

    def run(self):
        init_flag = True
        self.log_driver = self.parser.get_log_driver()
        self.all_ids = self.parser.get_all_containers_ids()
        self.containers, self.existed_ids = self.parser.get_existed_container_info()
        self.generate_config()
        self.kill_fluent()
        # self.restart()
        for e in self.docker_client.watch_event():
            LOG.info("event: {}".format(e))
            event = json.loads(e)
            if not event.get("status"):
                continue
            if init_flag:
                omit_containers, omit_ids = self.parser.get_omit_containers_info(self.existed_ids)
                self.containers.update(omit_containers)
                self.existed_ids.extend(omit_ids)
                init_flag = False
            else:
                if event["status"] == DOCKER_START:
                    id = self.docker_client.get_container_id(event)
                    self.existed_ids.append(id)
                    is_pause = self.parser.is_pause_container(id)
                    info = self.parser.get_tail_info(id)
                    # of couse ignore pause containers and containers without file log && fluend stdout
                    if is_pause or (not info and self.log_driver == "fluentd"):
                        continue
                    encoding = self.parser.get_log_encoding(id)
                    result = {
                        "encoding": encoding
                    }
                    self.all_ids[id] = result
                    if info:
                        self.containers[id] = info
                    self.generate_config()
                    self.kill_fluent()
                    # self.restart()
                elif event["status"] == DOCKER_DIE:
                    id = self.docker_client.get_container_id(event)
                    self.existed_ids.remove(id)
                    if id in self.all_ids.keys():
                        del self.all_ids[id]
                    if id in self.containers.keys():
                        del self.containers[id]
                        self.generate_config()
                        self.kill_fluent()
                        # self.restart()
                elif event["status"] == DOCKER_OOM:
                    id = self.docker_client.get_container_id(event)
                    url = "http://127.0.0.1:24225/docker.{}".format(id)
                    headers = {"content-type": "application/json"}
                    body = {
                        "container_id": id,
                        "source": "stderr",
                        "log": "Service out of memory. Please increase the container's memory."
                    }
                    try:
                        requests.post(url, headers=headers, data=json.dumps(body), timeout=30)
                    except Exception as ex:
                        LOG.error("Error: call flunted port 24225: {}".format(ex))
                else:
                    LOG.debug("Ignore event: {}".format(event))
