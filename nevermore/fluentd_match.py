import logging
import requests
import time
from jinja2 import Template
from template_handle import match_cfg
from settings import JAKIRO_ENDPOINT, REFRESH_INTERVAL, NAMESPACE, REGION_NAME, AUTH_TOKEN
from utils import restart
import hashlib

LOG = logging.getLogger(__name__)


class FluentOutputHandler(object):
    def __init__(self):
        self.jakiro_endpoint = self.getjakiroendpoint()
        self.namespace = NAMESPACE
        self.region_name = REGION_NAME
        self.interval = float(REFRESH_INTERVAL)
        self.request = requests.session()
        self.secret = ''

    def md5(self, data):
        m = hashlib.md5()
        m.update(data)
        return m.hexdigest()

    def factory(self, data):
        data = data['write_log_source']
        ret = {
            'default': '',
            'kafka': [],
            'elasticsearch': [],
            'fluentd_hosts': [],
            'logstash': []
        }
        for item in data:
            if not item.get('enabled', True):
                continue
            if item['type'] == 'default':
                ret['default'] = 'True'
            elif item['type'] == 'KafkaLogSource':
                ret['kafka'].append(item)
            elif item['type'] == 'ElasticsearchLogSource':
                ret['elasticsearch'].append(item)
            elif item['type'] == 'LogstashLogSource':
                integrated_address = item['integrated_address']
                logstashs = integrated_address.split(',')
                for logstash in logstashs:
                    l = logstash.split(':')
                    if len(l) != 2:
                        continue
                    ret['logstash'].append({
                        'host': l[0],
                        'port': l[1]
                    })
            elif item['type'] == 'FluentdLogSource':
                integrated_address = item['integrated_address']
                fluentds = integrated_address.split(',')
                for fluentd in fluentds:
                    f = fluentd.split(':')
                    if len(f) != 2:
                        continue
                    ret['fluentd_hosts'].append({
                        'host': f[0],
                        'port': f[1]
                    })
        return ret

    def getjakiroendpoint(self):
        pos = JAKIRO_ENDPOINT.find("/", 10)
        if pos == -1:
            return JAKIRO_ENDPOINT
        return JAKIRO_ENDPOINT[:pos]

    def run(self):
        LOG.debug('start to generate fluentd match configuration file')
        url = '{}/v1/regions/{}/{}/log_source'.format(self.jakiro_endpoint,
                                                      self.namespace,
                                                      self.region_name)
        headers = {
            "Authorization": "Token {}".format(AUTH_TOKEN)
        }

        rep = {
            'write_log_source': [
                {
                    'type': 'default'
                }
            ]
        }
        template = Template(match_cfg).render(self.factory(rep))

        with open('/etc/td-agent/match.conf', 'wb') as f:
            f.write(template)
        restart()
        time.sleep(self.interval)

        while True:
            try:
                rep = self.request.get(url, headers=headers)
            except Exception as e:
                LOG.error('Request {} failed: {}'.format(url, e))
                time.sleep(self.interval)
                continue

            if rep.status_code != 200:
                LOG.error('Request {} failed, status code: {}'.format(url, rep.status_code))
                time.sleep(self.interval)
                continue

            data = rep.text
            if self.secret == self.md5(data):
                time.sleep(self.interval)
                continue

            self.secret = self.md5(data)
            template = Template(match_cfg).render(self.factory(rep.json()))

            with open('/etc/td-agent/match.conf', 'wb') as f:
                f.write(template)
            restart()
            time.sleep(self.interval)
