#!/usr/bin/env python
import os
import time
BACKUP_PATH = "/var/log/mathilde/backup"
LIMIT_BACKUP_SIZE = float(os.getenv("LIMIT_BACKUP_SIZE", 1024))


def main():
    if os.path.exists(BACKUP_PATH) is False:
        return
    file_name_list = os.listdir(BACKUP_PATH)
    full_file_list = []
    total_size = float(0)
    for f in file_name_list:
        file_path = BACKUP_PATH + '/' + f
        t = os.path.getctime(file_path)
        s = os.path.getsize(file_path)
        temp_file = {
            "file_name": f,
            "ctime": t,
            "size": s/float(1024*1024)
        }
        full_file_list.append(temp_file)
        total_size += temp_file["size"]

    if total_size <= LIMIT_BACKUP_SIZE:
        return

    sorted_file_list = sorted(full_file_list, key=lambda x: x["ctime"])
    half_limit_backup_size = LIMIT_BACKUP_SIZE / 2.0
    for f in sorted_file_list:
        total_size -= f["size"]
        os.remove(BACKUP_PATH + '/' + f["file_name"])
        if total_size <= half_limit_backup_size:
            break


if __name__ == '__main__':
    while 1:
        main()
        time.sleep(60)
