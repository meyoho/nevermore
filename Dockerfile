FROM ubuntu:18.04
MAINTAINER Sincera xdzhang@alauda.io

RUN apt-get update -y && apt-get install sudo -y && \
    apt-get install curl -y && \
    apt-get install supervisor -y && \
    apt-get install logrotate -y && \
    apt-get install python-pip -y

# pick the architecture
RUN dpkgArch="$(arch)" \
  && case "${dpkgArch}" in \
    x86_64) curl -L https://toolbelt.treasuredata.com/sh/install-ubuntu-bionic-td-agent3.sh | sh && rm -rf /var/lib/apt/lists/*;; \
    aarch64) curl https://s3.amazonaws.com/packages.treasuredata.com/test/td-agent_3.5.1-0_arm64.deb -o td-agent_3.5.1-0_arm64.deb && dpkg -i td-agent_3.5.1-0_arm64.deb;; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac

RUN /usr/sbin/td-agent-gem install docker-api

RUN td-agent-gem install fluent-plugin-rewrite-tag-filter

RUN td-agent-gem install fluent-plugin-elasticsearch --no-document
RUN td-agent-gem install fluent-plugin-systemd -v 1.0.1
RUN td-agent-gem install fluent-plugin-kubernetes_metadata_filter -v 2.4.1
RUN td-agent-gem install fluent-plugin-multi-format-parser -v 1.0.0
RUN ln -s /nevermore/conf/supervisord.conf /etc/supervisord.conf
RUN ln -s /nevermore/conf/supervisord-crio.conf /etc/supervisord-crio.conf

RUN mkdir -p /alauda/logrotate /etc/supervisord/conf.d
RUN ln -s /nevermore/conf/supervisord-audit.conf /etc/supervisord/conf.d/supervisord-audit.conf
RUN ln -s /nevermore/conf/supervisord-event.conf /etc/supervisord/conf.d/supervisord-event.conf

COPY ./alauda_rotate.sh /alauda/logrotate

RUN chmod +x /alauda/logrotate/alauda_rotate.sh

COPY ./alauda_rotate /etc/logrotate.d/alauda

COPY ./requirements.txt /

RUN pip install -r /requirements.txt && rm -rf /root/.cache/pip/

COPY . /nevermore

ENV RETRY_LIMIT=4
ENV RETRY_WAIT=3s
ENV BUFFER_QUEUE_LIMIT=128
ENV BUFFER_CHUNK_LIMIT=64m
ENV TAIL_REFRESH_INTERVAL=5s
ENV HTTP_READ_TIMEOUT=60
ENV HTTP_OPEN_TIMEOUT=30

WORKDIR /nevermore

RUN chmod 755 /nevermore/*.sh

CMD ["/nevermore/run.sh"]
