require 'json'
module Fluent
  class DecodeJsonFieldsFilter < Filter
    Fluent::Plugin.register_filter('audit_msg', self)

    config_param :fields, :array, default: ["message"]
    config_param :target, :string, default: ""
    config_param :capitalize_first_char, :bool, default: true
    config_param :uncapitalize_parent_key, :array, default: ["spec", "annotations", "labels"]
    config_param :ignore_verbs, :array, default: ["list", "get", "watch"]

    def configure(conf)
      super
    end

    def start
      super
    end

    def shutdown
      super
    end

    def capitalize(k, v)
      # labels and annotations content should not capitalized
      if @uncapitalize_parent_key.include? k.downcase
        return v
      end
      if v.instance_of? Hash
        result = {}
        v.each do |key, value|
          result[key.slice(0,1).capitalize + key.slice(1..-1)] = capitalize(key, value)
        end
        return result
      end
      v
    end

    def filter(tag, time, record)
      for field in @fields do
        if !record.has_key? field
          next
        end
        begin
          data = JSON.parse(record.delete(field))
          if @target == ""
            data.each do |k,v|
              if @capitalize_first_char
                # k has no parent, so ignore the check of @uncapitalize_parent_key
                k = k.slice(0,1).capitalize + k.slice(1..-1)
              end
              record[k] = capitalize(k, v)
            end
          else
            record[@target] = {}
            data.each do |k,v|
              if @capitalize_first_char
                # k has no parent, so ignore the check of @uncapitalize_parent_key
                k = k.slice(0,1).capitalize + k.slice(1..-1)
              end
              record[@target][k] = capitalize(k, v)
            end
          end
        rescue
        end
      end
      # AIT-1523, add the missing ObjectRef.Name if RequestObject.Metadata.Name exists
      if record['Verb'] == 'create'
        objectRefName = record['ObjectRef']['Name']
        $log.info "handling create data with objectRefName: `#{objectRefName}`"
        if objectRefName == '' || objectRefName == nil
          $log.info "catch create without ObjectRef.Name, with Resource: #{record['ObjectRef']['Resource']}, with RequestObject: #{record['RequestObject']}"
          begin
            metadataName = record['RequestObject']['Metadata']['Name']
            if metadataName != '' && metadataName != nil
              $log.info "Update the ObjectRef.Name to #{metadataName}"
              record['ObjectRef']['Name'] = metadataName
            end
          rescue
          end
        end
      end
      cluster = ENV['__ALAUDA_REGION_NAME__']
      if cluster == nil
        cluster = ''
      end
      record['Cluster'] = cluster
      if @ignore_verbs.include? record['Verb']
        return nil
      end
      if @ignore_verbs.include? record['verb']
        return nil
      end
      begin
        record['ReferObject'] = record['ObjectRef']['Name']
      rescue
      end
      record
    end
  end
end
