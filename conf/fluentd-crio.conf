<source>
  @id fluentd-containers.log
  @type tail
  path /scan_dir/var/log/containers/*.log
  pos_file /scan_dir/var/log/alauda/containers.log.pos
  tag alauda.containers.*
  read_from_head true
  <parse>
    @type multi_format
      <pattern>
        format json
        time_key time
        time_format %Y-%m-%dT%H:%M:%S.%NZ
      </pattern>
      <pattern>
        format /^(?<time>.+) (?<stream>stdout|stderr) [^ ]* (?<log>.*)$/
        time_format %Y-%m-%dT%H:%M:%S.%N%:z
      </pattern>
  </parse>
</source>

# Enriches records with Kubernetes metadata
<filter alauda.containers.**>
  @id filter_kubernetes_metadata
  @type kubernetes_metadata
  de_dot true
  de_dot_separator &
</filter>

<filter alauda.containers.**>
  @id filter_kubernetes_labels
  @type kubernetes_labels
</filter>

<filter alauda.containers.**>
  @type record_transformer
  renew_record true
  keep_keys ["project_name", "application_name"]
  enable_ruby true
  auto_typecast true
  <record >
    node ${record["kubernetes"]["host"]}
    nodes ${record["kubernetes"]["host"]}
    region_name  "#{ENV['__ALAUDA_REGION_NAME__']}"
    region_id  "#{ENV['__ALAUDA_REGION_ID__']}"
    log_data ${record["log"]}
    log_level 0
    paths "stdout"
    file_name "stdout"
    time ${(time.to_f * 1000000).to_i}
    root_account alauda
    source container
    log_type log
    kubernetes_labels ${record["kubernetes"]["labels"]}
    kubernetes_namespace ${record["kubernetes"]["namespace_name"]}
    pod_name ${record["kubernetes"]["pod_name"]}
    pod_id ${record["kubernetes"]["pod_id"]}
    container_id ${record["docker"]["container_id"]}
    container_id8 ${record["docker"]["container_id"][0..7]}
    docker_container_name ${record["kubernetes"]["container_name"]}
    kubernetes_container_name ${record["kubernetes"]["container_name"]}
  </record>
</filter>

# output by common configuration with tag pattern alauda.*.**
@include /etc/td-agent/match.conf
