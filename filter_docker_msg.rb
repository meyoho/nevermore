require 'docker'
require 'socket'
require 'time'
require 'json'
module Fluent
  class PassThruFilter < Filter
    # Register this filter as "passthru"
    Fluent::Plugin.register_filter('docker_msg', self)

    # config_param works like other plugins

    def configure(conf)
      super
      # do the usual configuration here
      @app_info = Hash.new()
    end

    def start
      super
      # This is the first method to be called when it starts running
      # Use it to allocate resources, etc.
    end

    def shutdown
      super
      # This method is called when Fluentd is shutting down.
      # Use it to free up resources, etc.
    end

    def get_container_info(info)
      pod_name = nil
      pod_id = nil
      app_label = nil
      project_label = nil
      kubernetes_container_name = nil
      kubernetes_namespace = nil
      component = ''
      config = info["Config"]
      if config == nil
        return nil, nil, nil
      end
      envs = config["Env"]
      labels = config["Labels"]
      if envs == nil or envs.empty?
        return nil, nil, nil
      end

      region_name = '#ALAUDA_REGION_NAME#'
      region_id = '#ALAUDA_REGION_ID#'.gsub('-', '_')
      root_account = '#NAMESPACE#'
      app_label_key = '#APP_LABEL#'
      project_label_key = '#PROJECT_LABEL#'
      if region_name == ''
        region_name = nil
      end
      if region_id == ''
        region_id = nil
      end

      if app_label_key == ''
        app_label_key = 'app.alauda.io/name'
      end
      if project_label_key == ''
        project_label_key = 'project.alauda.io/name'
      end

      if config['Labels'].has_key? 'io.kubernetes.pod.name'
        pod_name = config['Labels']['io.kubernetes.pod.name']
      end
      if config['Labels'].has_key? 'io.kubernetes.pod.uid'
        pod_id = config['Labels']['io.kubernetes.pod.uid']
      end
      container_id = info['id']
      docker_container_name = info['Name']
      if config['Labels'].has_key? 'io.kubernetes.container.name'
        kubernetes_container_name = config['Labels']['io.kubernetes.container.name']
      end
      if config['Labels'].has_key? 'io.kubernetes.pod.namespace'
        kubernetes_namespace = config['Labels']['io.kubernetes.pod.namespace']
      end
      Docker.url = 'unix:///scan_dir/run/docker.sock'
      if config['Labels'].has_key? 'io.kubernetes.sandbox.id'
        sandbox_id = config['Labels']['io.kubernetes.sandbox.id']
        pause = Docker::Container.get(sandbox_id)
        pause_info = pause.info
        pause_config = pause_info["Config"]

        if pause_config['Labels'].has_key? app_label_key
            app_label = pause_config['Labels'][app_label_key]
        end

        if pause_config['Labels'].has_key? project_label_key
            project_label = pause_config['Labels'][project_label_key]
        end

        if pause_config['Labels'].has_key? 'tier'
          if pause_config['Labels'].has_key? 'component'
            component = pause_config['Labels']['component']
          end
        end
      end

      return {'container_id' => container_id, 'docker_container_name' => docker_container_name, 'pod_name' => pod_name,
       'pod_id' => pod_id, 'region_name' => region_name, 'region_id' => region_id, 'kubernetes_namespace' => kubernetes_namespace,
        'kubernetes_container_name' => kubernetes_container_name, 'root_account' => root_account, 'kubernetes_labels' => config['Labels'],
        'application_name' => app_label, 'project_name'=> project_label, 'component'=> component}
    end

    def filter(tag, time, record)
      # This method implements the filtering logic for individual filters
      # It is internal to this class and called by filter_stream unless
      # the user overrides filter_stream.
      #
      # Since our example is a pass-thru filter, it does nothing and just
      # returns the record as-is.
      # If returns nil, that records are ignored.

      tags = tag.split(".")
      log_type = tags[0]
      container_id = tags[1]
      temp_app_info = @app_info[container_id]
      Docker.url = 'unix:///scan_dir/run/docker.sock'

      if temp_app_info == nil
        if @app_info.has_key? container_id
          $log.warn "Can not find container #{container_id}. Ignore it"
          return nil
        else
          begin
            container = Docker::Container.get(container_id)
            @app_info[container_id] = get_container_info(container.info)
            temp_app_info = @app_info[container_id]
          rescue Docker::Error::NotFoundError => e
            $log.warn "Container #{container_id} not fount"
            @app_info[container_id] = nil
            temp_app_info = nil
          rescue Exception => e
            $log.warn "Get container Docker.url #{Docker.url} #{container_id} fail. class: #{e.class}, msg: #{e.message}"
          end
        end
      end

      if temp_app_info['pod_id'] == nil or temp_app_info['container_id'] == nil
        return nil
      end
      if temp_app_info['root_account'] == nil
        temp_app_info.delete('root_account')
      end

      record.merge!(temp_app_info)
      if log_type.eql? "json"
        log_data = JSON.parse(record["log"])
        record['log_data'] = log_data["log"]
        record['paths'] = "stdout"
        record["file_name"] = "stdout"
        record["log_type"] = "log"
      elsif log_type.eql? "docker"
        record['log_data'] = record["log"]
        record['paths'] = "stdout"
        record["file_name"] = "stdout"
        record["log_type"] = "log"
      elsif log_type.eql? "file"
        path =  Base64.decode64(tags[2])
        full_file = tags[3..-1].join(".")
        filename = full_file[path.length.. -1]
        filename_full_path = path + "/" + filename
        record["log_type"] = "file"
        record['paths'] = filename_full_path
        record["file_name"] = filename
      end
      temp_time = Time.now.utc
      record['time'] = (temp_time.to_f*1000000).to_i
      record['log_level'] = 0
      record['source'] = "container"
      record['node'] = Socket.gethostname
      record['@timestamp'] = temp_time.iso8601(6).to_s
      record.delete("log")
      record
    end
  end
end
