# encoding: utf-8
require 'fluent/plugin/output'

module Fluent
  # Main Output plugin class
  module Plugin
    class HttpBufferedOutput < Fluent::Plugin::Output
      Fluent::Plugin.register_output('http_buffered', self)
      def initialize
        super
        require 'net/http'
        require 'uri'
        require 'docker'
        require 'zlib'
        require 'openssl'
      end
  
      # Endpoint URL ex. localhost.local/api/
      config_param :endpoint_url, :string

      # statuses under which to retry
      config_param :http_retry_statuses, :string, default: ''

      # read timeout for the http call
      config_param :http_read_timeout, :float, default: 2.0

      # open timeout for the http call
      config_param :http_open_timeout, :float, default: 2.0

      # nil | 'none' | 'basic' | token
      config_param :authentication, :string, :default => nil
      config_param :username, :string, :default => ''
      config_param :password, :string, :default => '', :secret => true
      config_param :auth_token, :string, :default => ''

      def configure(conf)
        super

        # Check if endpoint URL is valid
        unless @endpoint_url =~ /^#{URI.regexp}$/
          fail Fluent::ConfigError, 'endpoint_url invalid'
        end

        begin
          @uri = URI.parse(@endpoint_url)
        rescue URI::InvalidURIError
          raise Fluent::ConfigError, 'endpoint_url invalid'
        end

        @auth = case @authentication
              when 'basic' then :basic
              when 'token' then :token
              when 'bearer' then :bearer
              else
                :none
              end

        # Parse http statuses
        @statuses = @http_retry_statuses.split(',').map { |status| status.to_i }

        @statuses = [] if @statuses.nil?

        @http = Net::HTTP.new(@uri.host, @uri.port)
        @http.read_timeout = @http_read_timeout
        @http.open_timeout = @http_open_timeout
        @http.use_ssl = (@uri.scheme == "https")
        @http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        @app_info = Hash.new()
      end


      def start
        super
      end

      def shutdown
        super
        begin
          @http.finish
        rescue
        end
      end

      def write(chunk)
        data = []
        chunk.each do |(tag, record)|
          data << record
        end

        request = create_request(data)

        begin
          response = @http.start do |http|
            request = create_request(data)
            http.request request
          end

          $log.info "return: #{response.code} msg: #{response.msg}. message: #{response.message}"
          if @statuses.include? response.code.to_i
          # Raise an exception so that fluent retries
            fail "Server returned bad status: #{response.code}"
          end
        # rescue IOError, EOFError, SystemCallError => e
        rescue Exception => e
          # server didn't respond
          $log.warn "Net::HTTP.#{request.method.capitalize} raises exception: #{e.class}, '#{e.message}'"
          fail "Other exception: #{e.class} #{e.message}"
        ensure
          begin
            @http.finish
          rescue
          end
        end
      end

      protected

        def create_request(data)
          request = Net::HTTP::Post.new(@uri.request_uri)

          # Headers
          request['Content-Type'] = 'application/json'
          if @auth and @auth == :basic
            request.basic_auth(@username, @password)
            end
          if @auth and @auth == :token
            request['Authorization'] = 'Token '.concat(@auth_token)
            end
          if @auth and @auth == :bearer
            request['Authorization'] = 'Bearer '.concat(@auth_token)
            end
          # Compress data
          request['Content-Encoding'] = 'gzip'
          request['Content-Type'] = 'text/gzip'
          # $log.info "data is #{data}"
          compress_data = Zlib::Deflate.deflate(Yajl::Encoder.encode(data), Zlib::BEST_SPEED)
          # Body
          request.body = compress_data
          request
        end
    end
  end
end