#!/bin/sh

/usr/local/bin/autopep8 --verbose --recursive --max-line-length 120 --in-place --aggressive --aggressive $1;

