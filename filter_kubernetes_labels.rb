require 'socket'
require 'time'
require 'json'
module Fluent
  class K8sLabelsFilter < Filter
    # Register this filter as "kubernetes_labels"
    Fluent::Plugin.register_filter('kubernetes_labels', self)

    # config_param works like other plugins

    def configure(conf)
      super
      # do the usual configuration here
    end

    def start
      super
      # This is the first method to be called when it starts running
      # Use it to allocate resources, etc.
    end

    def shutdown
      super
      # This method is called when Fluentd is shutting down.
      # Use it to free up resources, etc.
    end

    def get_labels_info(record)
      app_label = ""
      project_label = ""

      app_label_key = ENV['APP_LABEL']
      project_label_key = ENV['PROJECT_LABEL']
      ns_project_label_key = ENV['NS_PROJECT_LABEL']

      if app_label_key == '' or app_label_key == nil
        app_label_key = 'app.alauda.io/name'
      end
      if project_label_key == '' or project_label_key == nil
        project_label_key = 'project.alauda.io/name'
      end
      if ns_project_label_key == '' or ns_project_label_key == nil
        ns_project_label_key = 'alauda.io/project'
      end

      # with kubernetes_metadata with params: de_dot and de_dot_separator &
      app_label_key = app_label_key.gsub('.', '&')
      project_label_key = project_label_key.gsub('.', '&')
      ns_project_label_key = ns_project_label_key.gsub('.', '&')

      if record['kubernetes']['labels'].has_key? app_label_key
        app_label = record['kubernetes']['labels'][app_label_key]
      end
      if record['kubernetes']['labels'].has_key? project_label_key
        project_label = record['kubernetes']['labels'][project_label_key]
      else
        if record['kubernetes'].has_key? 'namespace_labels'
          if record['kubernetes']['namespace_labels'].has_key? ns_project_label_key
            project_label = record['kubernetes']['namespace_labels'][ns_project_label_key]
          end
        end
      end

      return {'project_name' => project_label, 'application_name' => app_label}
    end

    def filter(tag, time, record)
      # kubernetes and kubernetes.labels is a must
      if !record.has_key? 'kubernetes'
        return nil
      end
      if !record['kubernetes'].has_key? 'labels'
        return nil
      end

      container_id = nil
      if record.has_key? 'docker'
        if record['docker'].has_key? 'container_id'
          container_id = record["docker"]["container_id"]
        end
      end
      if container_id == nil
        return nil
      end

      label_info = nil
      label_info = get_labels_info(record)
      if label_info != nil
        record.merge!(label_info)
      end
      record
    end
  end
end
