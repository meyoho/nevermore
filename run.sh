#!/bin/bash
mkdir -p /var/log/mathilde
mkdir -p /var/log/td-agent
mkdir -p /buffer
if [[ $AUTHENTICATION = "bearer" ]];then
    sed -i "s|#APIGATEWAY_ENDPOINT#|$APIGATEWAY_ENDPOINT|g" /nevermore/nevermore/templates/template_*.py
    sed -i "s|#APIGATEWAY_URL#|$APIGATEWAY_URL|g" /nevermore/nevermore/templates/template_*.py
    sed -i "s|#APIGATEWAY_ENDPOINT#|$APIGATEWAY_ENDPOINT|g" /nevermore/conf/fluentd-audit.conf
    sed -i "s|#APIGATEWAY_AUDIT_URL#|$APIGATEWAY_AUDIT_URL|g" /nevermore/conf/fluentd-audit.conf
else
    sed -i "s|#APIGATEWAY_ENDPOINT#|$JAKIRO_ENDPOINT|g" /nevermore/nevermore/templates/template_*.py
    sed -i "s|#APIGATEWAY_URL#|$JAKIRO_API_URL|g" /nevermore/nevermore/templates/template_*.py
fi
sed -i "s|#AUTH_TOKEN#|$AUTH_TOKEN|g" /nevermore/nevermore/templates/template_*.py
if [ -z $AUTHENTICATION ];then
    sed -i "s|#AUTHENTICATION#|token|g" /nevermore/nevermore/templates/template_*.py
else
    sed -i "s|#AUTHENTICATION#|$AUTHENTICATION|g" /nevermore/nevermore/templates/template_*.py
fi
sed -i "s|#RETRY_LIMIT#|$RETRY_LIMIT|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#BUFFER_CHUNK_LIMIT#|$BUFFER_CHUNK_LIMIT|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#RETRY_WAIT#|$RETRY_WAIT|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#BUFFER_QUEUE_LIMIT#|$BUFFER_QUEUE_LIMIT|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#READ_TIMEOUT#|$READ_TIMEOUT|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#OPEN_TIMEOUT#|$OPEN_TIMEOUT|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#TAIL_REFRESH_INTERVAL#|$TAIL_REFRESH_INTERVAL|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#HTTP_READ_TIMEOUT#|$HTTP_READ_TIMEOUT|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#HTTP_OPEN_TIMEOUT#|$HTTP_OPEN_TIMEOUT|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#FLUSH_INTERVAL#|$FLUSH_INTERVAL|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#ALAUDA_REGION_NAME#|$__ALAUDA_REGION_NAME__|g" ./filter_docker_msg.rb
sed -i "s|#ALAUDA_REGION_ID#|$__ALAUDA_REGION_ID__|g" ./filter_docker_msg.rb

sed -i "s|#ALAUDA_REGION_NAME#|$__ALAUDA_REGION_NAME__|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#ALAUDA_REGION_ID#|$__ALAUDA_REGION_ID__|g" /nevermore/nevermore/templates/template_*.py
sed -i "s|#NAMESPACE#|$NAMESPACE|g" /nevermore/nevermore/templates/template_*.py

sed -i "s|#NAMESPACE#|$NAMESPACE|g" ./filter_docker_msg.rb
sed -i "s|#APP_LABEL#|$APP_LABEL|g" ./filter_docker_msg.rb
sed -i "s|#PROJECT_LABEL#|$PROJECT_LABEL|g" ./filter_docker_msg.rb

mv ./*.rb /etc/td-agent/plugin/
mv ./conf/fluentd-audit.conf /etc/td-agent/
mv ./conf/fluentd-event.conf /etc/td-agent/
sleep 1

CRIO_ENGINE="crio"

if [ "$CONTAINER_ENGINE" = "$CRIO_ENGINE" ]; then
    echo "Running in crio mode..."
    if [ -f "/etc/init.d/td-agent" ]; then
        rm -rf /etc/init.d/td-agent
    fi
    /usr/bin/supervisord -c /etc/supervisord-crio.conf --nodaemon
else
    cp -r td-agent /etc/init.d/
    chmod 755 /etc/init.d/td-agent
    echo "Running in docker mode..."
    /usr/bin/supervisord --nodaemon
fi